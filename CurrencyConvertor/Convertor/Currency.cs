﻿using System;

namespace Convertor
{
    public abstract class Currency
    {
        public double Value { get; set; }
        public Currency(double value)
        {
            this.Value = value;
        }

        public static Currency operator +(Currency firstCurrency, Currency secondCurrency) => firstCurrency.Add(secondCurrency);

        //Approach 1
        //var firstCurrencyType = firstCurrency.GetType().Name;
        //var secondCurrencyType = secondCurrency.GetType().Name;

        //var baseCurrency = ConfigHelper.GetStringValue("BaseCurrency");

        //var firstCurrencyUnitValue = ConfigHelper.GetDoubleValue(firstCurrencyType);
        //var secondCurrencyUnitValue = ConfigHelper.GetDoubleValue(secondCurrencyType);

        //firstCurrency.Value = firstCurrencyType.Equals(baseCurrency)
        //    ? ((firstCurrency.Value / secondCurrencyUnitValue) + secondCurrency.Value) * secondCurrencyUnitValue
        //    : firstCurrency.Value + (secondCurrency.Value / firstCurrencyUnitValue);

        //return firstCurrency;

        public abstract Currency Add(Currency currency);
        public override string ToString()
        {
            return $"{Math.Round(this.Value, 6)} {this.GetType().Name}";
        }
    }
}
