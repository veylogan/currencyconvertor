﻿using System;
using System.Configuration;
namespace Convertor
{
    public class ConfigHelper
    {
        public static double GetDoubleValue(string key)
        {
            double result = 0;
            try
            {
                var settingsValue = ConfigurationManager.AppSettings[key];
                if (string.IsNullOrEmpty(settingsValue)) return result;
                double.TryParse(settingsValue, out result);
            }
            catch (Exception)
            {
                result = 0;
                //TODO Logging
            }
            return result;
        }

        public static string GetStringValue(string key)
        {
            string result = null;
            try
            {
                var settingsValue = ConfigurationManager.AppSettings[key];
                if (string.IsNullOrEmpty(settingsValue)) return result;
                result = settingsValue;
            }
            catch (Exception)
            {
                result = null;
                //TODO Logging
            }
            return result;
        }
    }
}
