﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Convertor;
namespace CurrencyConvertorUI
{
    class Program
    {
        static void Main(string[] args)
        {
            //var Usd5 = new USD(5);
            //var Rs1000 = new INR(1000);

            //var usdConverted = Usd5 + Rs1000;
            //Console.WriteLine($"5 USD + 1000 INR : {usdConverted.ToString()} ");

            //Usd5 = new USD(5);
            //var inrConverted = Rs1000 + Usd5;
            //Console.WriteLine($"1000 INR + 5 USD : {inrConverted.ToString()} ");

            int digits = 100;
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            string formattedCurrency = digits.ToString("C");

            CultureInfo taIN = new CultureInfo("ta-IN");
            var dateTimeString = DateTime.Now.ToString(taIN);

            Console.ReadLine();
        }
    }
}
